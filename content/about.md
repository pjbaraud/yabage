---
title: "About"
date: 2017-09-14T16:11:50+01:00
---

About Me
========

My name is Pierre-Jean Baraud. I'm a French Software Engineer living in London, UK. I'm passionate about Software Craftsmanship, open source, DIY, hacking, fun creations and heated discussions. I like to share with my colleagues and friends about new trends in software technologies, hear about their latest coolest discoveries or sit down and wonder about the meaning of life.

If you want to contact me, [drop me a mail][mailme].

[mailme]: pierre-jean@baraud.fr
