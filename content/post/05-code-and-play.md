---
title: "Code and play"
date: 2014-05-08T20:46:08+02:00
categories: ["Practice"] 
---

Feel disconnected from the pleasure of code? Hacking is definitely less fun than what you expected as a teenager when watching Matrix?
Well, it's time to enjoy the fun of coding again, thanks to one of the projects I'm going to present in this article!

Untrusted, a meta-JavaScript adventure game
===========================================

Untrusted is a project developed by [Alex Nisnevich][alexnisnevich], where you play in an ASCII art environment and interact in it with JavaScript code.

![untrusted-screenshot][] 

This project was acclaimed by the community, and it is well deserved! You can try it [here][untrusted], and get the source code on [github][untrusted-src]. At the time of the writing of this article, the project has received 1518 stars from the github community, and has been forked 197 times!

<!--more-->

As I am speaking of fork, I want to introduce the initiative of my friend [Janos Gyerik][janosgyerik], [HangoverX][hangoverx], that will allow you to go on enjoying playing this meta-JavaScript adventure game with a complete re-written storyline.

CodinGame, game solving challenges
==================================

[CodinGame][codingame] is a platform where you can enjoy many different challenges: Kirk's Quest, Skynet Revolution, The Last Crusade... The only way to win? Code the best solution!

The platform set up challenges during a limited period of time, but I strongly suggest to try they practice multiplayer games, available at anytime, to play with their nice web interface and train your skills! For instance, you can play to Tron battle, and code the best algorithm to be the last one on the ring!

![tron-screenshot][] 

You will be able to choose your prefered language: Bash, C, C#, C++ Clojure, Dart, Go, Groovy, Haskell, Java, Javascript, ObjectiveC, Pascal, Perl, PHP, Python (2 & 3), Ruby and Scala!

You can train your coded bot against the default AI, but the real interesting part is when you put it to the arena and fight against all other player algorithms, and/or choose some selected player to fight with!  

Note that many challenges are sponsorized by game or software [companies][cg-companies] that use these challenges to detect new talents! So, having fun and practicing your craft can actually benefit your career! what are you waiting for?

[alexnisnevich]: http://alex.nisnevich.com/portfolio/
[untrusted]: https://alexnisnevich.github.io/untrusted/
[untrusted-src]: https://github.com/AlexNisnevich/untrusted/
[janosgyerik]: http://www.janosgyerik.com/
[hangoverx]: https://github.com/janosgyerik/hangoverx
[codingame]: http://www.codingame.com/cg/
[cg-companies]: http://www.codingame.com/cg/#!contact

[untrusted-screenshot]: /images/post/05-code-and-play/untrusted-screenshot.png
[tron-screenshot]: /images/post/05-code-and-play/gaming/tron-screenshot.png
