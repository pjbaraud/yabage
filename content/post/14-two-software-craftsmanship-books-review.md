---
title: "Software Craftsmanship: take back control"
date: 2016-07-18T23:16:54+01:00
comments: true
categories: ["Books","Software Craftsmanship"]
---

![cover-battle][]

Today I am gonna give you a quick review of the last two books I read: [The Clean Coder][clean-coder], by _Robert C. Martin_ (aka _Uncle Bob_), and [The Software Craftsman][software-craftsman], by _Sandro Mancuso_.
Both books cover the same topic: the **Software Craftsmanship** movement.

<!--more-->

The Software What ?
===================

Remember my [first blog post about passion][first-blog-post] ? Well, if I had to rewrite it today I would title it "Be passionate, be a Software Craftsman".

Yes, Software Craftsmanship is all about passion. But it is also much more than that, and taught me a lot about my own shortcomings.

Passion
-------

I'm going to be shamelessly lazy and quote Sandro's book:

{{< blockquote author="Sandro Mancuso" source="The Software Craftsman" >}}
Passion. That summarizes it all. Software craftsmen are passionate about software development and their profession. They are passionate about solving problems with simple solutions. They are passionate about learning, teaching and sharing. Software craftsmen are passionate about helping the software industry to evolve; sharing their code; mentoring young developers; sharing their experiences via blogs, books, talks, videos, and conversations; and being active in technical communities.
{{< /blockquote >}}

Yes, I know, he just expressed above the exact content of my first blog post, but in a clearer and more efficient way... Thanks for pointing that out! But even if I can't compete with Sandro's writing style, at least we are on the same page regarding the importance of passion!

Professionalism
---------------

But Software Craftsmanship is also about _professionalism_, as clearly demonstrated by the subtitle of the first book _"A Code of Conduct for Professional Programmers"_, and of the second one _"Professionalism, Pragmatism, Pride"_. If the message wasn't clear enough, you will find a reference to professionalism in every paragraph, if not sentence.

And this is where my previous outlook fell short, and the first big lesson I learnt. Before reading these books, I figured business (the business world) and passion (true passion) to be natural opposites. I would've suggested to embrace your hobby and to not care about the corporate guys in suits that would crush your dream trying to make you do "just a job". In a word: I was immature.
I thought that success would come if you were motivated enough, and that only start-up companies promoting a cool-t-shirt-dress-code understood our industry. I was blaming everyone except myself for issues and problems within failing projects, companies or culture environments.

My behaviour was the opposite to that of a Software Craftsman. The Software Craftsmanship movement is all about taking responsibility, centring on involvement in the project and in the company you are working for, and spreading the culture of passion within new technical disciplines.

Disciplines and Values
----------------------

Speaking of disciplines: you may expect these books to teach you a set of disciplines to apply in order to succeed in any situation. And even if it does mention a set of practices (e.g. _Test Driven Development_, _Pair programming_ or _SOLID principles_), these books are *not* about these disciplines. These books are about *values*. We're talking about a long term vision for our industry, the mistakes that are made and how some current disciplines and attitudes can solve these common issues. 

And many other things
---------------------

You want to know more? Good. Go read the books. Now.

Robert Cecil Martin and Sandro Mancuso are both great writers and you will not only gain a great deal from reading them, but enjoy it too. 

If you still need convincing, you can watch one of the public presentations the authors gave (for instance [here][sandro-talk] and [there][uncle-bob-talk]) more or less covering the same ground as the books. The value of these books is not so much about the topic but how they illustrate each idea through different examples that speak to us all.

Why are these books so important?
=================================

You may ask: do I need to go through the hundreds of pages in two books to figure out such an obvious message as "be passionate and professional" ? Well, look around and we see reality demonstrating on a daily basis that it is not as obvious as it seems.

A simple fact
-------------

Have you ever been slowed down in your projects by software evolutions that were waiting to be delivered? Have you ever been appalled by the numbers of broken features each time a software is released?

And as a developer, have you ever felt mistrust towards a business industry that does not understood you?

Chances are that yes, you have. The sickness of the software industry cannot be more visible than it is now and the truth is society has no clear idea on how to fix it. Millions are wasted by companies because of this situation. And it's precisely the intention of Software Craftsmanship to provide a way out.

A necessary bridge
------------------

A cultural gap exists between business managers and software developers. Business sees developers as a production line, a technical pipe without much value except providing the expected result. By expected result, I mean the one specified by business managers and software architects, the ones creating value through specifications. That's why in many companies, the only career evolution available is to switch from the _technical position_ of developer to a _management position_ such as business manager, architect, and so on. This illustrates the poor consideration management usually has towards developers. On the other hand, considerable distrust exists in software departments towards the management, the clients, and basically anyone that is not a developer. We confine ourselves behind technical considerations that appear out of reach to everyday people.

This split of values also affects the developers' profession. Many are divided in choosing between their career (promoting: business, management, architecture, specific-technologies) or their passion (coding, open-source, choosing new technologies, sharing). Software Craftsmanship is a way to do both. Software craftspeople don't see their activity as a production line. They see it as a craft. They know that companies who attempted to outsource their software department to low-skilled unknown coders failed. Specifications and upstream architectural decisions are not enough. Software craftspeople bring value to companies. They offer the flexibility needed by companies to follow their business' evolution. And this requires high skilled developers, impassioned and proud of the results they can provide.

A day to day transformation
---------------------------

Who owns your career? Software craftsmanship offers you a chance to take back control of your working environment, your career and your passion.

These two books provide a pragmatic approach to change your habits and enhance your working conditions. For instance, by changing the language you use, you will know how to identify the lack of accountability expressed in your daily discussions. You will learn how to take responsibility and communicate it to your colleagues.

You will learn how to face pressure without it affecting your discipline. You will gain insights on how to work efficiently with the different characters and singularities you can encounter in your team.

Each concept of the book can be applied in your current job starting today. The value they will bring will be immediate and visible. Chances are you will find yourself much happier doing the same job with the same persons than you may have been.

Food for the mind
=================

If not both, then reading either one of the books will certainly achieve the same result: open up new perspectives, provide a big dose of motivation and reconcile yourself with many situations you would have defined as problematic before.

Whilst reading you will learn how to raise yourself to the level of software craftsman without sacrificing your ethics or your personal life. But these books may also leave you with a strange feeling. They may open doors without you noticing. By questioning your work environment, you will also challenge your definition of happiness, your goals in life and maybe even rediscover your own identity ... It's obviously not the purpose of Software Craftsmanship books to answer such philosophical questions, but be prepared to face them.

[clean-coder]:http://www.informit.com/store/clean-coder-a-code-of-conduct-for-professional-programmers-9780137081073
[software-craftsman]:http://www.informit.com/store/software-craftsman-professionalism-pragmatism-pride-9780134052502
[first-blog-post]:http://pierre-jean.baraud.fr/blog/2013/08/10/be-passionate/
[sandro-talk]:https://www.youtube.com/watch?v=9OhXqBlCmrM
[uncle-bob-talk]:https://www.youtube.com/watch?v=9Xy3QC7yxJw

[cover-battle]: /images/post/14-two-software-craftsmanship-books-review/cover-battle.png
