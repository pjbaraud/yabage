---
title: "Be passionate, no matter what"
date: 2013-08-10T16:46:47+02:00
type: "post"
categories: ["Culture"]
---

Live your passion...now
=======================

I am a cautious person. I like to weigh pros and cons before making a choice. I always prevailed adaptability over personal beliefs. But if I learned one thing in the last few years, it's that being **good is all about passion**.

There are plenty of average software developers considering development as a job. There is nothing wrong about that. But if you really want to learn and progress, you should focus on people who live their passion.

<!--more-->

Don't be afraid to take the risk of following your dream. If you really do what you believe in, you will be 200% more efficient! I know this might sound naive, but it's not: you only have one life, don't realize too late it's not the one you want.

You have the potential to improve
=================================

Courage is about trusting yourself. Don't hide your weakness. **Share** what you know. Focus on what you have to learn: there is a world of knowledge waiting for you, and you should be excited about it!

I'm a truly bad example on this, but it's never too late to change: open-source projects are a very cool way to become better. It's about taking initiatives, accepting unpleasant remarks, learning and improving!

Read, read, read and participate!
=================================

There is a looong list of fascinating people out there who inspire me every day. But for this post, I will focus on one: Jeff Atwood, the guy who created the Stack Exchange framework (and the well-known website that use it: [stackoverflow.com][1]). [His blog][2] and [his books][3] really impacted me on how to become better on what I do. (for French speakers, some articles from [this blog][4] will have the same spirit of motivation).

Encouraged by some friends, I decided to start a blog, because it's a good exercise for a developer: writing instead of reading, producing instead of consuming. It forces involvement in your passion, and I really hope to learn about this experience.

If you don't believe me...believe him!
--------------------------------------

On a lighter note, I let Schwarzenegger sum up my speech:

{{< youtube vH0nP4NzS9M >}}

[1]: https://stackoverflow.com
[2]: https://www.codinghorror.com/blog/ "Coding Horror Blog"
[3]: https://www.codinghorror.com/blog/2012/07/coding-horror-the-book.html "Books section of coding horror"
[4]: https://vincent.jousse.org/ "Blog de Vincent Jousse"
