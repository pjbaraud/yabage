---
title: "SCL 2017 talks in one sentence"
date: 2017-11-21T16:41:06+01:00
comments: true
categories: ["Conference", "Software Craftsmanship"]
---

![sc-london-logo][]

The [Software Craftsmanship London 2017][scl2017], or _SCL_, was a 2 days conference organized between the 5/10/2017 and 6/10/2017. I had the chance to assist this event. I'll try below to juice out one sentence per talk and explain why I chose this one.

The selected quote doesn't summarize the talk nor is it supposed to be the most important for each presentation. It is just to give you a taste with one example of something said that I liked and encourage you to have a look to [the complete videos of the talks][scl-videos].

<!--more-->

Software Crafsmanship - 8 Years
===============================

The opening talk has been done by _Sandro Mancuso_, famous figure of Software Craftsmanship, creator of the [London Software Craftsmanship Community][LSCC] (aka _LSCC_), and co-founder of [Codurance][codurance].
He took a step back on the progress of the Software Craftsmanship community over the last 8 years and what it has brought to our industry.

{{< youtube G2UoAPcgs9o >}}

If I needed to keep one sentence from this talk, it would be:

{{< blockquote author="Sandro Mancuso" >}}
Although I'm very happy with everything that we have done as an industry up until now, when I look at the wider picture I realize that we are still quite a tiny bubble.
{{< /blockquote >}}

[Extract][sandro-quote]

Indeed, the current state of an industry is really hard to measure objectively because our _vision_ of the industry is closely related to our current environment. By evolving within a community that promotes good practices and people that enhance your skills, you may confuse the progression you did as a Software Craftsman and the state of the industry itself. Yet, there is no doubt the Software Craftsmanship movement is growing quickly.

What we talk about when we talk about software
==============================================

_Nat Pryce_, coauthor of [Growing Object-Oriented Software Guided by Tests][growing-objects-guided-by-tests], gave a really interesting talk on the use of metaphors to represent concepts in our industry.

{{< youtube syLjjmRgTsE >}}

There was a lot to keep from this presentation but as the exercice is to choose one, I'll quote this part:

{{< blockquote author="Nat Pryce" >}}
For everyone who stuck on the same metaphor and you notice that metaphor, you can start thinking about different metaphors that can provide a different view point onto the way everybody is thinking and talking about things and then maybe, people come up with different decisions and different ideas might start emerging.
{{< /blockquote >}}

[Extract][nat-quote]

The whole talk was fascinating and guided us through the force and weaknesses of metaphors. I was really struck when I realize the power of the _representation_ of a reality when we choose to model it. How the choice of the representation could enable or prevent us to think it through some specific way. For instance, by _drawing_ a set of collaborating entities differently, by choosing circles instead of squares, linear representation instead of matrix, your brain will come up with totally different ideas.

The Novoda Craftsmanship University
===================================

_Paul Blundel_, head of Engineering at [Novoda][novoda], provided a really refreshing and inspiring presentation of Novoda internal training process: the Novoda Craftsmanship University.

{{< youtube RUC_W8HsT4M >}}

I found the process in place at Novoda Craftsmanship University impressive, intelligent and sensible. For instance, I really liked the following quote:

{{< blockquote author="Paul Blundel" >}}
It's not that you are a master, you have a mastery of a skill or a part of your job.
{{< /blockquote >}}

[Extract][paul-quote]

Indeed I found quite a common shortcoming of our society to measure and compare people on a specific metric or having a one-dimension way to measure things.
And it is then easy to fall into a really simple grid for viewing the world. By acknowledging and keep in mind that we are measuring a specific set of skills and not a person, we also remove the feeling of judgment. And with it go the common Impostor syndrome. You may be less experienced/performant compare to some people in some skills but you will also be more experienced/performant in another set. Human beings are incredibly complex beings and there is definitely no way to score them.

TDD as if you meant it
======================

_Alex Bolboaca_, Software Craftsman at [Mozaic Works][mozaic-works], introduced us to the set of constraints known as _TDD as if you meant it_, created by _Keith Braithwaite_, through a live step by step example.

{{< youtube gNmsWP1LKKs >}}

Through the deliberate practice of _TDD as if you meant it_, a lot of things emerge and one of them is:

{{< blockquote author="Alex Bolboaca" >}}
A class is nothing more than a set of cohesive partially applied pure functions.
{{< /blockquote >}}

[Extract][alex-quote]

The process of TDD as demonstrated by Alex is a really slow process where each action is delibarate and where constants are turned into variables which are in turn transformed to methods. It was enlightening to witness that the choice of a programming paradigm such as _OOP_ vs _Functionnal programming_ could be delayed so far. The practice of TDD as if you meant it requires discipline but seems to bring a lot when facing doubts about directions to go.

Get Kata
========

_Kevlin Henney_, coauthor of [97 Things Every Programmer Should Know][97-things-book] and prolific public speaker on Software Development, gave a dynamic and funny talk on the practice of Katas.

{{< youtube dHPX1SzeDjE >}}

Kevlin is a master of punchlines and many could have been chosen. As I need to keep one, I'll retain the below:

{{< blockquote author="Kevlin Henney">}}
What we often consider to be bad code is not arbitrary, is not malicious, it's actually systematic: it follows a particular set of habits.
{{< /blockquote >}}

[Extract][kevlin-quote]

This sentence really reminds me how my own bad code was the produce of an "automatic-mode" of coding. When you are a young developer especially, it is easy to follow rules, to believe in the "right" way of doing things (which is often the new way, the last one your discovered). The result is your design is not guided by your brain but by a systematic application of a set of rules which often end up in monsters projects.

Usable Software Design
======================

_Johan Martinsson_, Agile coach and consultant, demonstrated how we should consider code in term of usability and how to get there.

{{< youtube GdqjX-bF7HI >}}

One of his introduction sentence really stayed in my mind long after the conference:

{{< blockquote author="Johan Martinsson" >}}
Talking about solutions is really conflictual. Talking about the problems can unite things.
{{< /blockquote >}}

[Extract][johan-quote]

He demonstrated that a bit later in his presentation with a concrete example. This is something that is really useful to keep in a conscious box when dealing with complicated situations, even outside the scope of Software projects.

WebOps for the Overloaded Developer
===================================

_Samir Talwar_, Chief Technology Officer at [prodo.ai][], gave a live demo about a webapp deployment and the tools anyone can use to automate admin operations.

{{< youtube 8rWp50YhcB4 >}}

One of the common trap we can fell into as an admin and that Samir pointed at is:

{{< blockquote author="Samir Talwar" >}}
What value does code add? Nothing. Code is a cost.
{{< /blockquote >}}

[Extract][samir-quote] 

Indeed, we may have been many to fall into this situation when administrating our own server and we may have end up developing dedicated fine grained solution. But this knowledge is quickly lost if you are not an admin on a daily base and the solution become technical debt when you need to fix a specific problem or need to upgrade something months or years after.

Confessions of a developer community builder
============================================

_John Stevenson_, VP of Equity Derivative Data Fabric at [citi][], and also opiniated developer on functional languages that you can meet in many London Software Craftsmanship events, gave his feedback on how to develop a community (being himself involved in many of them).

{{< youtube Y-UR6Wf_17I >}}

Within the many things he mentioned, the one I choose because I also experienced it a lot is:

{{< blockquote author="John Stevenson" >}}
Having a face to face community experience is really good for helping you strengthen any communication you're gonna have when you do go online as well.
{{< /blockquote >}}

[Extract][john-quote] 

Despite all the technology, all the processes, nothing will replace social and human interaction. We tend to forget this aspect of collaboration and all the bias and influence coming from it.
We are not robots, and our choice are not always rational.

Understanding Understandability
===============================

_Michael Feathers_, author of [Working effectively with legacy code][working-effectively-with-legacy-code], shared his view about what is understandability in code.

{{< youtube oUswpgf5V9M >}}

The quote I selected is:

{{< blockquote author="Michael Feathers" >}}
When you have these high levels operations, they provide you different ways of conceptualizing your problems.
{{< /blockquote >}}

[Extract][michael-quote]

I used to believe that readability was in a way universal and you should follow simple rules to make your code understable.
I never really focused on the grey area presented here by Michael where the complexity of the language needed to be taken into account in the eyes of the level of you colleagues. It was intersting to see how striving for concisness could be considered as harmful to the understandability of the code (for some users).

Go-ing there and back again
===========================

_Steve Freeman_, coauthor of [Growing Object-Oriented Software, Guided by Tests][growing-objects-guided-by-tests], presented his work on a Go project where tests became difficult to maintain. 

{{< youtube bRqIcYR4dcU >}}

The presentation is a stage by stage demonstration of the different steps of the refactoring. It is hard to sum up in one sentence (but is really valuable to watch). One thing he mentioned that I'll try to remember:

{{< blockquote author="Steve Freeman" >}}
If you have an idea, do the experiment and find out if it's true or not.
{{< /blockquote >}}

[Extract][steve-quote]

As he explained before, when coding in Agile process we may fall into this failure mode where you believe that every line of code should be related to a feature delivery. It is sometimes important to take a step back and _experiment_ an idea (before taking the decision of going full on with it).

The day after tomorrow: Let the machine do the coding
=====================================================

_Nicole Rauch_, Software developer, coach in software development and one of the original creator of [Socrates conference][socrates], and _Michael Sperber_, COO at [Active Group][active-group] that hosts the [BOB conference][bob-conf], performed an amusing show about functional languages and why we need better types to create safer systems. 

{{< youtube CO95FShDySA >}}

The demonstration was really interesting but I really think the important part was the point Michael made during the questions phase:

{{< blockquote author="Michael Sperber" >}}
You should think about properties and the right programming language lets you write those properties down.
{{< /blockquote >}}

[Extract][nicole-and-michael-quote]

I don't have much experience with fully functional languages but I found this approach of thinking programs really interesting.

Craft Thinking
==============

_Mashooq Badar_, co-founder of [Codurance][codurance], gave a talk about the the meaning of what we approach as a metaphor when talking about Software Craftsmanship. Let me be clear: this is maybe one of the most significant talk on Software Craftsmanship I have heard so far. Choosing one sentence is almost impossible as every slide was pure gold for me. Each single idea resonated with me and had a huge impact on how to approach my field and see my industry. It was a really inspiring talk and maybe the reason I took the time to write this blog post.

{{< youtube AjDcPOydSM8 >}}

One idea that was tremendously important for me was:

{{< blockquote author="Mashooq Badar" >}}
Leveraging the strenghts and the weaknesses of that material and understanding where you are constrained is actually part of pragmatism.
But it is a force. It helps you create the best thing. A perfect thing.
{{< /blockquote >}}

[Extract][mashooq-quote]

This quote may not seem as inspiring as it should and do not give credit to the full talk but it really resolved one of my main conflict with what we usually call "Pragmastism". The sentence is part of a metaphor on material focus approach and shows how pragmatism is not a way to lower your standard or to make concession with quality. It is actually how you pull all your knowledge and expertise (your craft, in a way) to work towards the best solution including the constraints of the real world (the material here). I can't stress enough how important this presentation is and you should definitely watch it.

The importance of syntax in clean code
======================================

_Hadi Hariri_, VP of Developer Advocacy at [JetBrains][jetbrains], hijacked the SCL conference to promote Kotlin. And we loved it.

{{< youtube pAFiPjXEOtg >}}

To explain why using a language like Kotlin play a role in enhancing your code, he used the following argument:

{{< blockquote author="Hadi Hariri" >}}
Syntax in my opinion contributes to clutter and noise and if Clean Code is about comprehension, then it matters.
{{< /blockquote >}}

[Extract][hadi-quote]

Kotlin is gaining a lot of fame and has managed to become quickly adopted by a vast community. And watching the perfectly executed demonstration of its great features as it has been done in this talk can only convert the coder I am.

Learning, Communication, Quality, and Craftsmanship
===================================================

_Joe Schmezer_, programmer and agile coach, gave us a feedback on how to manage Agile projects and what influence the success of this mindset.

{{< youtube 7_LXnQCeMTA >}}

Among the many thing he mentioned, one of his advice was:

{{< blockquote author="Joe Schmetzer" >}}
Expose your thought process cause one of the biases is that:
"Wait, People understand what I am doing. I understand you, you understand me. It's clear what I'm thinking." It's not.
{{< /blockquote >}}

[Extract][joe-quote]

I liked it cause he also made a lot of references to [Nonviolent Communication][nvc], and this is clearly part of the process of NCV. To be honest, I had the book on my shelf for quite a while before this conference but all the mention on NVC by different speaker finally triggered the will to read it, and it changed a lot of things on the way I approach communication.

Closing Keynotes
================

_Rachel Davies_, lead engineer at [Makers academy][makers-academy] and author of [Agile coaching][agile-coaching], presented tips on how to integrate good practices or start promoting things learnt during the conference in your office environment. A really well executed talk full of insights:

{{< youtube QJVFpog2ehs >}}

There are many things to keep but I'll choose that one:

{{< blockquote author="Rachel Davies" >}}
If you try and lead everything yourself, people will think you are trying to get overcredit and that actually is one of the things that acts against trust.
{{< /blockquote >}}

[Extract][rachel-quote]

This is something I experienced a lot. The association between an idea or a project with an individual make things personal. It is then easy for the one carrying the idea to feel rejected when his idea is not accepted. And the other way around, to presents the idea as his baby which can make people defensive about it.

Conclusion
==========

It was a very good conference with amazing speakers. If you are interested, there will be [another edition next year][scl2017]. If you want to react on things said during this conference feel free to leave a comment.

[scl2017]: http://sc-london.com/
[scl-videos]: http://sc-london.com/videos 
[LSCC]: https://www.meetup.com/london-software-craftsmanship/
[codurance]: https://codurance.com/
[sandro-quote]: https://youtu.be/G2UoAPcgs9o?t=1675
[growing-objects-guided-by-tests]: http://growing-object-oriented-software.com/
[nat-quote]: https://youtu.be/syLjjmRgTsE?t=2021
[novoda]: https://www.novoda.com/
[paul-quote]: https://youtu.be/RUC_W8HsT4M?t=529
[mozaic-works]: https://mozaicworks.com/
[alex-quote]: https://youtu.be/gNmsWP1LKKs?t=1515
[97-things-book]: http://shop.oreilly.com/product/9780596809492.do
[kevlin-quote]: https://youtu.be/dHPX1SzeDjE?t=915
[johan-quote]: https://youtu.be/GdqjX-bF7HI?t=44
[prodo.ai]: https://prodo.ai
[samir-quote]: https://youtu.be/8rWp50YhcB4?t=1477
[citi]: http://www.citigroup.com/citi/
[john-quote]: https://youtu.be/Y-UR6Wf_17I?t=1081
[working-effectively-with-legacy-code]: https://www.amazon.fr/Working-Effectively-Legacy-Michael-Feathers/dp/0131177052
[michael-quote]: https://youtu.be/oUswpgf5V9M?t=1800
[steve-quote]: https://youtu.be/bRqIcYR4dcU?t=2418
[socrates]: https://www.socrates-conference.de/
[active-group]: http://www.active-group.de/
[bob-conf]: http://bobkonf.de/2018/en/
[nicole-and-michael-quote]: https://youtu.be/CO95FShDySA?t=2678
[mashooq-quote]: https://youtu.be/AjDcPOydSM8?t=1225
[jetbrains]: https://www.jetbrains.com/
[hadi-quote]: https://youtu.be/pAFiPjXEOtg?t=1977
[joe-quote]: https://youtu.be/7_LXnQCeMTA?t=2444
[nvc]: https://cnvc-bookstore.myshopify.com/collections/general-nvc/products/nonviolent-communication-a-language-of-life-3rd-edition-now-25percent-off
[makers-academy]: http://www.makersacademy.com/ 
[agile-coaching]: https://pragprog.com/book/sdcoach/agile-coaching
[rachel-quote]: https://youtu.be/QJVFpog2ehs?t=956

[sc-london-logo]: /images/post/15-scl-2017-summary/scl-logo-round.jpg
