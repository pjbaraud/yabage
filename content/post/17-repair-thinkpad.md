---
title: "Repairing an old thinkpad with broken hinge"
date: 2020-08-15T00:00:00+01:00
comments: true
categories: ["Repair", "DIY"]
---

<div class="youtube-container" style="position:relative;width:100%;height:0;padding-bottom: 56.25%;">
<iframe class="youtube-video" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://www.youtube.com/embed/CocswUVXk60" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

I'm a big fan of tinkering with electronic devices and regularly attempt to repair phones, laptops or computers around me. A few weeks ago I decided to repair an old thinkpad with a broken hinge by installing a fresh new bottom cover (after different previous attempts at saving it in a more hacky way). Above is my recording of the session.

<!--more-->

This is somehow a very basic operation that only requires patience and is accessible to novices. If you like this kind of content, I have plenty of topic to cover:

 - Hacking around a 3D printer
 - Arduino projects
 - Repairing simple electronic devices
 - Refurbishing old computers

I will try to record more often my projects to make them available online.

[cover_conf]: /images/post/17-repair-thinkpad/cover.webp
