---
title: "Train your Java skills"
date: 2014-05-12T21:04:02+02:00
categories: ["Practice"] 
---

Want to train your Java developer skills? Having an interview to prepare? Just want to refresh your memory on theory or evaluate your current knowledge?

Here are some sites to help you.

<!--more-->

Questions sets
==============

You can find on internet many websites with some interview questions. Here are two I personally selected:

Udemy Top 15 questions article
------------------------------

[Udemy][udemy-site] wrote an [article about the top 15 questions][udemy-article] asked on Java interviews. This article presents really classic questions that you pretty sure to be asked about, or at least for which you should know the answer before going to an interview.

JavaTpoint 170 core Java questions article
------------------------------------------

If you want a more extended set of question than the previous article, you can read [the 170 core Java questions article][javatpoint-article] by [JavaTpoint][javatpoint-site].

This questions cover a large part of core Java (basic) knowledge, and they come with nicely written answers.

Interactive problems
====================

Finally, a language knowledge is nothing if you don't master algorithms. [Codility][codility-site] is a website that aims to be used by companies to select candidates through coding exercices, but they provide [a nice training section][codility-training] where you can practices your skills on exercices and problems. 

![codility-screenshot][]

It's free, and really well designed!

If you know any other source of training, feel free to share them!

[udemy-site]: https://www.udemy.com/blog
[udemy-article]: https://www.udemy.com/blog/java-interview-questions/
[javatpoint-site]: https://www.javatpoint.com/
[javatpoint-article]: https://www.javatpoint.com/corejava-interview-questions
[codility-site]: https://codility.com/
[codility-training]: https://codility.com/programmers/lessons/

[codility-screenshot]: /images/post/07-train-your-skills/codility-screenshot.png
