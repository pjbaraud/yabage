---
title: "What if you host a conference... in your own room?"
date: 2019-09-01T00:00:00+01:00
comments: true
categories: ["Conference", "Software Craftsmanship"]
---

![cover_conf][]

If you have been lucky enough to go to conferences, hackathons, code-retreats or unconferences, you must know how much 
these events are privileged moments to learn and grow in your field. But they come with a cost: you may need to 
take days off to join them, they need to be available at a date and location compatible with your situation and not be 
sold out when you finally decide to pick one. So today, let’s reverse the table: if you can’t go to the conference of 
your dream, why not make the conference of your dream ...comes to you? 

<!--more-->

What are unconferences?
=======================

I will be focusing in this article on a very particular type of event: **unconferences**.
An unconference is a participant driven meeting and you may have heard the term if you are in the software industry as 
they are pretty popular from my experience in this community. You may also have heard them called **Open Space**. 
My first unconference was at [Socrates][socrates] and I can really recommend to anyone who has no experience with an 
unconference to start with that one: you should find one organized in your country and chances are it will blow 
your mind. Socrates is often run over 3 days on a remote place (hotel in the countryside, castle, etc) and you will have
lots of opportunities to interact with the group present at the event.

![socrates_marketplace][]

I have since then enjoy unconferences in different shapes and sizes, from company-scale ones to … friends self-hosted 
ones (also called *Open House*).

How do they work?
================

The typical structure of an unconference is to gather attendees in one location for one or several days and start each 
day with the **marketplace**. During the marketplace, people meet in front of a board displaying a grid, each line 
representing a time slot of the day and each columns a space that the location offers for the event. And this is 
where the magic happens as it’s up to the participant to fill that board with sessions.

![scunconf_marketplace][]

Each participant that wants to run a session will use the marketplace to present succinctly what the session is about 
to the rest of the attendees and stick the name of the session at the time and location they chose on the board. 
The session can be a presentation already prepared, a topic someone is curious about and want to discuss with others,
 a hands-on experiment, a game or any kind of interaction you wish of. At the end of the marketplace, the board will 
 become the reference for the schedule of the unconference and people will often refer to it to decide which session to 
 join. At the end of the day, a **closing circle** is often organized where attendees share a description of the session
 they have attended to and/or what they have learnt from it.

Why is it cool?
===============

I’m still often impressed about how interesting unconferences are. One could think that sessions run by the participants
 may drop the level of the content compared to talks provided by speakers. Or others could believe that the message 
 would be diluted in chaotic interactions between the contributors. I have experienced the contrary. I have been 
 recently to a “classic conference” and I was quite frustrated when realizing that some presentations were below 
 the expectations suggested by their description. During an unconference, all the knowledge that
 participants already have will add to the session instead of building frustration. The format of the session, ad hoc 
 and without top down structure does not bring chaos but on the contrary is really efficient to provide the right level
  of information that is pertinent for the group: discussion is a very efficient way with short feedback loop to agree on what 
  we know and dissipate unclear ideas. Finally, the collaborative effort to tackle a topic and run the event shift 
  the mindset and atmosphere of the whole experience. There are some special bonds that are made during unconferences
   that are hard to create in other types of events.

That seems amazing… but in a flat, really?
=========================================

Ok so you may be convinced on the benefits of unconferences. But you may wonder how realistic it can be to run them in a flat? 
I used to see how easily these events could scale up but I had a hard time to picture 
how they could scale down… but they actually do really well! I think the configuration of my flat played a role but if 
you believe your flat does not offer an ideal situation to host such event, do not worry too much: it’s much more about the 
people than it is about the space. I have a 2 bedrooms flat which with the living room provides 3 rooms 
for sessions + a kitchen if people do not feel like participating in an event for some time. Indeed,  it’s often the case
 that people initiate a discussion that they want to finish so having a ‘neutral’ 
 space is a good idea. A garden can also do the trick. I had around 15 people last time I organised an unconference 
 which basically gives an occupation of 5 persons per room. I am planning to invite more people for the next ones so I
  may update this article to let you know how it goes. The important thing is that the feedback I received was phenomenal.

![sessions][]

I think the reason why people enjoyed it so much comes from a set of specific characteristics of Open Houses. I have the feeling
that self-hosted unconferences, on top of many of the benefits from a classic unconference, provide the extra advantage 
of offering a really safe space: you can often talk more freely than in a public or professional environment. 
It also feels almost like a family when you are such a small number in such a private space and the boundary 
between personal and professional vanish. Finally, people realize how special these events are and somehow how fragile or
ephemeral they can be. It demonstrates a strong motivation from everyone involved to keep them running as you are literally 
in someone’s house and everyone is here on his free time to share their passion.

Ok, so what should I know before running one?
=============================================

![marketplace]

If you are interested in hosting one, here is my feedback of things to keep in mind.

 - *Space needed*: I believe the minimum amount of rooms you need to have is 2 spaces + a neutral space to run an 
 unconference. A nice dynamic of unconferences is that it is not just a linear succession of talks: people need to split
  in different groups every hour to decide which session they want to go to. Everybody has a different experience at 
 the end of the day. I imagine that providing just one space would alter that feeling that is for me the DNA of unconferences.
 So one room and a living room would work. 
 Having a kitchen or a garden on top is great for people that want to skip a session and socialize.
 
 - *Setup of space*: your flat is probably not meant to host unconferences. I had to reorganize the space and furniture
  to provide 3 comfortable rooms where people could sit (even if sometimes they prefer to stand). That’s where I was lucky, 
  readjusting beds as sitting places, providing lots of chairs in other rooms and moving tables around appear to provide 
  3 perfect configurations for presentations.
  
![bedroom]

 - *Material*: Most of my rooms are filled with windows, so I didn’t have places for boards. Nevertheless I used some 
 windows as boards by writing on them directly with pen for whiteboard surface (it wipes out well). You can also use 
 self adhesive sheets (you can find them under the name of magic whiteboard online):  it is a bit more pricey but much 
 more comfortable for the eye depending on what is behind your window. I also put a lot of focus on trying to provide 
 most of the room with projectors. I believe it really helps as most people will want to share their laptops screen at 
 some point. If you have a big monitor or TV it will work fine too. Try also to have the adaptors for different types 
 of connections. I used either white wall or window blinds as a screen for projectors and it worked really well. 
 But don't worry too much. All of these things are just nice to have. If you really scale down your event and end up being 
 only 2 or 3 per room, you don’t need any board or display.
 
 - *People*: It is a delicate topic I believe. I put a special attention while selecting the people I invite because 
 motivation of the attendance is key in such a scale down version of an unconference. You don’t have as much room for 
 adjustment as you could with a wider audience. Because traditional unconferences are usually made of larger
  groups, it allows them to be more relaxed on the ratio *people offering a session* vs *people attending a session*.
  In an Open House, if you only have a very small percent of sessions suggested, the schedule can quickly feel empty.
  So I usually try to find the right balance of profiles so that everybody can benefit from the experience and still a 
  lot of sessions are run. It’s a delicate challenge and I don’t have a golden rule for that one yet.
  
 - *Food*: as the event is run on a full day, you will want to think about food. I went for pizzas, because they do not 
 require much cutlery and you can easily adjust to different diets. You will want to ask about allergies or specific 
 diets if you are the one ordering the food. I chose the pizzas to be delivered because you want something that arrive 
 at the right time all together and didn’t want the overhead of preparing food for such a big group while sessions were 
 being run. You can also ask people to bring in their own food (even less overhead needed). Because the people I invited
  are very dear to me, I decided to cover the price of the food and beverage. This may add up quickly though: I went 
  for nice local pizzeria and bought some good selection of beers and it was quite a budget to be honest. To make it 
  sustainable, you need to find a system that works for everybody. Most people that attend my open house were actually
  happy to chip in so I may go for that solution on next iterations.

![pizzas][]

Conclusion
==========
So should you do it? I really recommend the experience. I had the chance to be invited to an open house before deciding 
to host one myself and I think these kind of days are very special. People can feel you put your heart into it and always 
render it in one form or another. If you decide to host one or already have done it please leave your feedback in the 
comments as it will benefit anyone reading that article (and myself ;-) ).
The hardest step is always the first step: so just pick a date with some friends or colleagues and start the adventure!

[socrates]: https://socratesuk.org/

[socrates_marketplace]: /images/post/16-open-house/socrates_marketplace.jpg
[scunconf_marketplace]: /images/post/16-open-house/scunconf_marketplace.jpg
[sessions]: /images/post/16-open-house/sessions.webp
[marketplace]: /images/post/16-open-house/marketplace.webp
[pizzas]: /images/post/16-open-house/pizzas.webp
[bedroom]: /images/post/16-open-house/bedroom.webp
[cover_conf]: /images/post/16-open-house/conf.jpg
