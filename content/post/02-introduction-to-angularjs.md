---
title: "Introduction to AngularJS"
date: 2013-08-17T12:13:23+02:00
categories: ["Programming"]
---

![angular-logo][]

I discovered recently this exciting framework, and even if I'm still a beginner in it, I presented it to my colleagues to share my enthusiasm.
Here are the slides i used for the presentation.

<!--more-->

<div style="left: 0; width: 100%; height: 0; position: relative; padding-bottom: 74.9296%;"><iframe src="https://speakerdeck.com/player/7a7a33c0bb5301309b3a023921bdbc68" style="border: 0; top: 0; left: 0; width: 100%; height: 100%; position: absolute;" allowfullscreen scrolling="no" allow="encrypted-media"></iframe></div>

[angular-logo]: /images/post/02-introduction-to-angularjs/angularjs-logo.jpg 
